import {createContext, useState} from 'react';

const CartContext = createContext({
    cart: [],
    totalitems: 0,
    totalPrice: 0,
    setCart: (itemObj) => {},
    setTotalItems: (totalItems) => {},
    setTotalPrice: (totalPrice) => {},
    computeTotalQty: (data) => {},
    getCartData: () => {}
});

export function CartContextProvider(props) {
    const [cart, setCart] = useState([]);
    const [totalItems, setTotalItems] = useState();
    const [totalPrice, setTotalPrice] = useState();

    const getCartData = () => {
        setCart(prevCart =>{
            fetch( `${process.env.REACT_APP_API_URL}/order/cart/get`,
            {
               method: 'GET',
               headers:{
                  'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
               }
            })
             .then(res => res.json())
             .then(data => {
                return data.cart
             })
        })
    }

    const computeTotalQty = (data) => {
        // Spread cart array into temp variable
        const cart = [...data.cart];

        // Get each item quantity in cart
        const cartQty = cart.map(itemObj => itemObj.quantity);

        // Get the sum of the array
        const reducer = (previousValue, currentValue) => previousValue + currentValue;
        const totalItems = cartQty.reduce(reducer);

        // Save total
        setTotalItems(totalItems);
    }

    const cartContext = { 
        cart: cart,
        totalItems: totalItems,
        totalPrice: totalPrice,
        setCart: setCart,
        setTotalItems: setTotalItems,
        setTotalPrice: setTotalPrice,
        computeTotalQty: computeTotalQty,
        getCartData: getCartData
    }

    return <CartContext.Provider value={cartContext}>
        {props.children}
    </CartContext.Provider>
}

export default CartContext;