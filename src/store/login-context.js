import {createContext, useState} from 'react';

const LoginContext = createContext({
    user: ()=>{},
    setUser: ()=>{},
    unsetUser: ()=>{}
});

export function LoginContextProvider(props) {
    const [user, setUser] = useState({
        accessToken: localStorage.getItem('accessToken'),
        email: localStorage.getItem('email'),
        role: localStorage.getItem('role')
    });

    const unsetUser = () => {
        localStorage.clear();
        setUser({
          accessToken: null,
          email: null,
          role: 'guest'
        })
    }

    const loginContext = {
        user: user,
        setUser: setUser,
        unsetUser: unsetUser
    }

    return <LoginContext.Provider value={loginContext}>
        {props.children}
    </LoginContext.Provider>
}

export default LoginContext;