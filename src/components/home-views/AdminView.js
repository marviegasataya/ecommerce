export default function AdminView(){
    return(
        <div>
            <h1>This is the Admin View</h1>
            <ul>
                <li>Retreive list of all products (Available or Unavailable)</li>
                <li>Create New Product</li>
                <li>Update Product Information</li>
                <li>Deactivate/Reactivate Product</li>
            </ul>
        </div>
    )
}