import {Container, Row, Col, Jumbotron, Button, Card} from 'react-bootstrap'
import classes from './UserView.module.css'
import {Link} from 'react-router-dom'

export default function GuestView(){

    const divStyle = {
        color: 'white',
        boxShadow:'inset 0 0 0 2000px rgba(0, 45, 20, 0.3)',
        backgroundImage: ' linear-gradient(to bottom, rgba(245, 246, 252, 0.4), rgba(117, 19, 93, 0.73)), url(https://i.imgur.com/td8ZcuY.jpg)',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'right center',
        position: 'relative',
        height: '500px',
        marginLeft: 'auto',
        marginRight: 'auto'
      };

    return(
        <Container>
            <div>
                
            </div>
            <Jumbotron fluid style={divStyle}>
                <Container className="d-flex h-100">
                    <div className="mt-auto pl-4" style={{maxWidth:'450px'}}>
                        <h1 className="pb-3" style={{textShadow: '2px 2px 15px rgba(0, 0, 0, 1), 2px 2px 5px rgba(0, 0, 0, 0.7), ', fontWeight:'bold'}}>Garden Space</h1>
                        <p  className="pb-2" style={{textShadow: '2px 2px 5px rgba(0, 0, 0, 0.7), 2px 2px 5px rgba(0, 0, 0, 0.7)', fontSize:'1.6em', fontWeight:'bold', letterSpacing: "2px"}}>
                            Your one-stop-shop for all your gardening needs.
                        </p>
                        <Button variant="success" className="p-3" style={{fontSize: "1.2em", fontWeight: 'bold'}}>
                            View All Products
                        </Button>
                    </div>
                </Container>
            </Jumbotron>

            <Row>
                <Col className="d-md-flex justify-content-between">
                    <Link to={`/products/category/seeds`}>
                        <Button className={`p-2 ${classes.buttonStyle}`}>
                            <Card className="text-center shadow" style={{maxWidth:'350px'}}>
                                <Card.Img variant="top" src="https://i.imgur.com/ud4FJap.jpg" style={{maxWidth: 'auto', height: '350px'}}/>
                                <Card.Footer className="text-muted">Seeds</Card.Footer>
                            </Card>
                        </Button>
                    </Link>

                    <Link to={`/products/category/containers`}>
                        <Button className={`p-2 ${classes.buttonStyle}`}>
                            <Card className="text-center shadow" style={{maxWidth:'350px'}}>
                                <Card.Img variant="top" src="https://i.imgur.com/oiB8032.jpg" style={{maxWidth:'auto', height: '350px'}}/>
                                <Card.Footer className="text-muted">Containers</Card.Footer>
                            </Card>
                        </Button>
                    </Link>

                    <Link to={`/products/category/hydroponics`}>
                        <Button className={`p-2 ${classes.buttonStyle}`}>
                            <Card className="text-center shadow" style={{maxWidth:'350px'}}>
                                <Card.Img variant="top" src="https://i.imgur.com/6M1A1IF.jpg" style={{maxWidth:'auto', height: '350px'}}/>
                                <Card.Footer className="text-muted">Hydroponics</Card.Footer>
                            </Card>
                        </Button>
                    </Link>

                </Col>
            </Row>

            <Row>
                <Col className="d-md-flex justify-content-between">
                    <Link to={`/products/category/soil`}>
                        <Button className={`p-2 ${classes.buttonStyle}`}>
                            <Card className="text-center shadow" style={{maxWidth:'350px'}}>
                                <Card.Img variant="top" src="https://i.imgur.com/TLu9JsO.jpg" style={{maxWidth: 'auto', height: '350px'}}/>
                                <Card.Footer className="text-muted">Soil</Card.Footer>
                            </Card>
                        </Button>
                    </Link>

                    <Link to={`/products/category/soil`}>
                        <Button className={`p-2 ${classes.buttonStyle}`}>
                            <Card className="text-center shadow" style={{maxWidth:'350px'}}>
                                <Card.Img variant="top" src="https://i.imgur.com/SzFd0Qi.jpg" style={{maxWidth: 'auto', height: '350px'}}/>
                                <Card.Footer className="text-muted">Fertilizers</Card.Footer>
                            </Card>
                        </Button>
                    </Link>

                    <Link to={`/products/category/tools`}>
                        <Button className={`p-2 ${classes.buttonStyle}`}>
                            <Card className="text-center shadow" style={{maxWidth:'350px'}}>
                                <Card.Img variant="top" src="https://i.imgur.com/uZQlzpj.jpg" style={{maxWidth: 'auto', height: '350px'}}/>
                                <Card.Footer className="text-muted">Tools</Card.Footer>
                            </Card>
                        </Button>
                    </Link>
                </Col>
            </Row>

            {/* <div>
                Featured
            </div> */}
        </Container>
    )
}