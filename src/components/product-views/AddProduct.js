import React, { useState, useEffect, useRef } from "react";
import {Button, Table, Modal, Form, OverlayTrigger, Tooltip} from 'react-bootstrap';
import Swal from 'sweetalert2';
import classes from './AdminView.module.css'

export default function AddProduct({closeAdd, showAdd, fetchData}){
    // Set Useref variables
        const nameInputRef = useRef();
        const priceInputRef = useRef();
        const stockQtyInputRef = useRef();
        const descriptionInputRef = useRef();
        const brandInputRef = useRef();
        const imageURLInputRef = useRef();
        const attributeInputRef = useRef();
    //Get A reference to the form
        const formEl = useRef();

    // Select Control handlers
        // Stores the Category Select Input value
        const [valueCat, setValueCat] = useState("default");
        // Stores the Subcategory Select Input value
        const [valueSubcat, setValueSubcat] = useState("default");
        const [isSubcatDisabled, setIsDisabled] = useState(true);
        // Prepares the Subcategory options JSX needed for rendering
        const [subCatOptionsJSX, setSubCatOptionsJSX] = useState([]);
        const [subCatOptionsData, setsubCatOptionsData] = useState([]);
        // Prepares the Images Input Additions JSX needed for rendering
        const imageCountInputRef = useRef();
        const [totalImages, setTotalImages] = useState(1);
        const [isAddImage, setIsAddImage] = useState(false);
        const [imageInputFeildsJSX, setimageInputFeildsJSX] = useState([]);
        const imageInputRefs = useRef([]);
        const imageAttributeInputRefs = useRef([]);
        imageInputRefs.current = [];
        imageAttributeInputRefs.current = [];



    // Creates an array of JSX Input feilds based on totalImages
    // On addImageFields or removeImageFields triggers this effect
    useEffect(()=>{
        let imageFeilds = [];
        const imageFeildCount = totalImages-1;
        
        // Value to populate the JSX with at least 1 imput feild
        const firstImageInput = (
            <tr className='mb-1' key={0}>
                <td colSpan={1} className={classes.numberLeft}>
                    <p className={classes.centerP}>1</p>
                </td>
                <td colSpan={9} className="pb-3">
                    <Form.Label column='sm'>Image Url: </Form.Label>
                    <Form.Control type="text" size='sm' placeholder="Add URL path of image" ref={imageURLInputRef} required/>
                    <Form.Label column='sm'>Image description: </Form.Label>
                    <Form.Control type="text" size='sm' placeholder="Add alt description" ref={attributeInputRef} required/>
                </td>
                <td colSpan={2} className='d-flex flex-column align-items-center'>
                    <Button size='sm' className={(totalImages >= 1) ? classes.hideButton : 'mt-2'} onClick={removeImageFields}>
                        X
                    </Button>
                </td>
            </tr>
        );

        imageFeilds.push(firstImageInput)

        for(let x = 0; x < imageFeildCount; x++){
            // console.log(`adding image field`);
            imageFeilds.push(
                <tr className='mb-1' key={x+2}>
                    <td colSpan={1} className={classes.numberLeft}>
                        <p className={classes.centerP}>{x+2}</p>
                    </td>
                    <td colSpan={9} className="pb-3">
                    <Form.Label column='sm'>Image Url: </Form.Label>
                    <Form.Control type="text" size='sm' placeholder="Add URL path of image" ref={addToImageRefs} required/>
                    <Form.Label column='sm'>Image description: </Form.Label>
                    <Form.Control type="text" size='sm' placeholder="Add alt description" ref={addToImageAttributeRefs} required/>
                    </td>
                    <td colSpan={2} className='d-flex flex-column align-items-center'>
                        <Button size='sm' className={(totalImages >= x+3) ? classes.hideButton : 'mt-2'} onClick={removeImageFields}>
                            X
                        </Button>
                    </td>
                </tr>
            )
        }
        setimageInputFeildsJSX([...imageFeilds]);
    }, [totalImages]) // eslint-disable-line react-hooks/exhaustive-deps

        

        // Keeps track of total count of image feilds, decreases count
        const removeImageFields = (e) => {
            if(totalImages-1 < 8){
                setIsAddImage(false);
            }
            setTotalImages(totalImages - 1);
        }

        // dynamically creates and adds a reference attribute for a newly added input feils
        const addToImageRefs = (el) => {
            if(el && !imageInputRefs.current.includes(el)){
                imageInputRefs.current.push(el);
            }
        }
        
        // dynamically creates and adds a reference attribute for a newly added input feils
        const addToImageAttributeRefs = (el) => {
            if(el && !imageAttributeInputRefs.current.includes(el)){
                imageAttributeInputRefs.current.push(el);
            }
        }

        // Keeps track of total count of image feilds, increases count
        const addImageFields = (e) => {
            let additions = parseInt(imageCountInputRef.current.value);
            let runningTotal = totalImages;

            if(additions < 0){
                additions = 0;
            } 
            runningTotal += additions;
            if(runningTotal > 8){
                runningTotal = 8;
                setIsAddImage(true);
            }
            setTotalImages(runningTotal);
        }


    // Creates an array of JSX select options based on subCatOptionsData
    // Onchange of Category, the handleCategoryChange triggers which triggers this effect
    // since subCatOptionsData changes in handleCategoryChange() function
    useEffect(()=>{
        const optionsArr = subCatOptionsData.map(option =>{
            return(
                <option key={option} value={option}>{option}</option>     
            )
        });
        setSubCatOptionsJSX([...optionsArr]);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [subCatOptionsData])

    // When Category Option value has been selected
    // It enables the Subcategory Select Input
    // It Stores the selected value in setValueCat
    const handleCategoryChange = (e) => {
        setValueCat(e.target.value);
        setsubCatOptionsData([]);
        let subcategoryTitles = [];
        switch(e.target.value){
            case 'Seeds':
                subcategoryTitles.push("Flowers");
                subcategoryTitles.push("Vegetables");
                subcategoryTitles.push("Fruits");
                break;
            case 'Containers':
                subcategoryTitles.push("Pots");
                subcategoryTitles.push("Seed/Grow bag");
                subcategoryTitles.push("Seedling Tray");
                break;
            case 'Hydroponics':
                subcategoryTitles.push("Hydroponics Kit");
                subcategoryTitles.push("Hydroponics Solution");
                subcategoryTitles.push("Hydroponics Accessories");
                break;
            case 'Soil & Amendments':
                subcategoryTitles.push("Soil & Growing Medium");
                subcategoryTitles.push("Solid Fertilizer");
                subcategoryTitles.push("Liquid Fertilizer");
                break;
            case 'Tools':
                subcategoryTitles.push("Grow Lights");
                subcategoryTitles.push("Shears");
                subcategoryTitles.push("Wires, Ties & Cages");
                subcategoryTitles.push("Watering Cans");
                break;
            default:
        }
        setsubCatOptionsData([...subcategoryTitles]);
        setIsDisabled(false);
    };

    // Collects the value from the Subcategory Input Select Option
    const handleSubcategoryChange = (e) => {
        setValueSubcat(e.target.value);
    };


    // Actions Taken Upon Submit
    // Save in DB, Clear Form, Close Modal & Sweet Alert Pop up
    const submitHandler = (e) => {
        e.preventDefault();

        // Clean data iput from gathered references
        const name = nameInputRef.current.value.trim();
        const price = priceInputRef.current.value;
        const stockQty = stockQtyInputRef.current.value;
        const description = descriptionInputRef.current.value.trim();
        const brand = brandInputRef.current.value.trim();

        // Store All Image Urls in a Javascript Object
        let imageURLs = [imageURLInputRef.current.value.trim()];
        let imageAttributes = [attributeInputRef.current.value.trim()];

        // loop through added references for image URLS
        imageInputRefs.current.forEach((el, index) => {
            imageURLs.push(el.value);
        })

        // loop through added references for image attributes
        imageAttributeInputRefs.current.forEach((el, index) => {
            imageAttributes.push(el.value);
        })

        // create a Javascript object for images
        let imageObjects = [];

        for(let x = 0; x < imageURLs.length; x++){
            let imageData = {
                imageUrl: imageURLs[x],
                attribute: imageAttributes[x]
            }
            imageObjects.push(imageData);
        }
        // create a Javascript object to be used in the body's submission:
        const formData = {
            name: name,
            price: price,
            stockQuantity: stockQty,
            details: {
                description: description,
                brand: brand,
                category: valueCat,
                subCategory: valueSubcat,
                images: [...imageObjects]
            }
        }
        // Add data to database
        fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
            method: 'POST',
            headers:{
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify(formData)
        })
        .then( response => response.json())
        .then(data => {
            if(data.success){
                
                Swal.fire({
                    title: data.title,
                    icon: 'success',
                    text: data.message
                });
                formEl.current.reset();
                setTotalImages(1);
                imageInputRefs.current = [];
                imageAttributeInputRefs.current = [];
                closeAdd();
                fetchData();
            }else{
                Swal.fire({
                    title: data.title,
                    icon: 'error',
                    text: data.message
                });
            }  
        });

       
    };

    return(
        <>
            {/* { MODAL HERE } */}
            <Modal show={showAdd} onHide={closeAdd} >
                <Modal.Header closeButton>
                        <Modal.Title>Add Product</Modal.Title>
                </Modal.Header>
                <Form onSubmit={submitHandler} ref={formEl}>
                    <Modal.Body>
                        <Form.Group>
                            <Form.Label >Name</Form.Label>
                            <Form.Control type="text" placeholder="Enter name of product" size="sm" ref={nameInputRef} required/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label >Brand</Form.Label>
                            <Form.Control type="text" placeholder="Enter brand name" size="sm" ref={brandInputRef} required/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Price</Form.Label>
                            <Form.Control type="number" size="sm" ref={priceInputRef} required/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Stock Qty</Form.Label>
                            <Form.Control type="number" size="sm" ref={stockQtyInputRef} required/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Category</Form.Label>
                            <Form.Control as="select" size="sm" defaultValue={valueCat} onChange={handleCategoryChange}>
                                <option  value="default" disabled hidden>Select Category</option>
                                <option>Seeds</option>
                                <option>Containers</option>
                                <option>Hydroponics</option>
                                <option>Soil & Amendments</option>
                                <option>Tools</option>
                            </Form.Control>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Subcategory</Form.Label>
                            <Form.Control as="select" size="sm" defaultValue={valueSubcat} disabled={isSubcatDisabled} onChange={handleSubcategoryChange}>
                                <option  value="default" disabled hidden>Select Subcategory</option>
                                {subCatOptionsJSX}
                            </Form.Control>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Description</Form.Label>
                            <Form.Control type="text" as="textarea" size="sm" placeholder="Enter product description" ref={descriptionInputRef} required/>
                        </Form.Group>
                        <Form.Group>
                            <Table responsive="sm"  size='sm'>
                                <thead>
                                    <tr>
                                        <th colSpan={12}>Images:</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    {imageInputFeildsJSX}
                                    <tr>
                                        <td colSpan={1} className="d-flex">
                                            <OverlayTrigger
                                                placement={'bottom'}
                                                overlay={
                                                    <Tooltip id={`max image warn`}>
                                                    8 images max.
                                                    </Tooltip>
                                                }
                                            >
                                            <Form.Control type="number" size="sm" ref={imageCountInputRef} className={classes.imageCountInput} defaultValue={1} />
                                            </OverlayTrigger>
                                        </td>
                                        <td colSpan={11}>
                                            <Button size='sm' className='mt-0' onClick={addImageFields} disabled={isAddImage}>
                                                Add More Fields
                                            </Button>
                                            <p className={(totalImages === 8) ? `d-block ${classes.warnMessageP}` : 'd-none'}>max reached</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </Table>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeAdd}>Close</Button>
                        <Button variant="success" type="submit">Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    )
}