import { useState, useEffect} from 'react'
import {Link, useLocation, useHistory } from 'react-router-dom'
import {Container, Card, ListGroup, Spinner, Breadcrumb} from 'react-bootstrap';
import classes from './UserView.module.css'
import DisplayReview from '../DisplayReview'

export default function UserView({productsData, fetchData}){
    const history = useHistory();
    const [JSX, setJSX] = useState();
    const location = useLocation();
    const lastSegmentString = location.pathname.split("/").pop();
    const [lastSegment, setLastSegment] = useState();
    const [isLoading, setIsLoading] = useState(true);

    useEffect( ()=>{
        setLastSegment(lastSegmentString);

        // Filters selection based on category
        const filteredArr = productsData.filter(product => product.details.category.toLowerCase() === lastSegmentString || lastSegmentString === 'all');

        if(filteredArr.length !== 0 ){
            const productsArr = filteredArr.map(product => {
                return(
                    <div className="col col-lg-3 mt-3 w-100" key={product._id} >
                        <Link to={`/products/item/${product._id}`}>
                            <button className={`${classes.button}`}>
                                <Card className="card shadow-sm ml-auto mr-auto" style={{ width: '14em' }} onClick={() => {
                                        history.push(`/products/item/${product._id}`);
                                }}>
                                    <div style={{ height: '14em' }}>
                                        <Card.Img variant="top h-100 w-100" src={`${product.details.images[0].imageUrl}`}/>
                                        <div className={`${classes.overlay}`}>
                                            <div className={`${classes.text}`}>View Product</div>
                                        </div>
                                    </div>
                                    <hr className="m-0"/>
                                    <Card.Body className="card-body m-0 p-1" style={{ height: '7em' }} >
                                        <h5 className="card-title mt-0 mb-0">{product.name}</h5>
                                        <div className="p-0 mt-1">
                                                <p className="m-0"> {product.details.brand}</p>
                                                <p className="m-0 font-weight-bold"> PHP {product.price} </p>
                                                <DisplayReview rating={product.ratings} size={15} className="mr-2"/>                                              
                                        </div>
                                    </Card.Body>
                                </Card>
                            </button>
                        </Link>
                    </div>
                )
            });
            setJSX(productsArr);
            setIsLoading(false);
        }else {
            if(productsData.length !== 0){
                const noItems = (<div>No items Found</div>);
                setJSX(noItems);
                setIsLoading(false);
            }
        }

    }, [productsData, lastSegmentString, history]);

    const CategoryChange = ()=> {
        setLastSegment(location.pathname.split("/").pop());
    }

    let loadingSpinner = '';

    if(isLoading){
        loadingSpinner = 
        <Container className='d-flex flex-column justify-content-center align-items-center p-5'>
            <div>
                <Spinner animation="border" style={{color: "green"}}/>
            </div>
            <div className='ml-2 mt-2'>
                <p>Loading...</p>
            </div>
        </Container>
    }


    return(
        <div  className={`text-center mt-4 ${classes.container}`} >
            {/* Note: Make the BreadCrumb into a component that accepts props */}
        <Breadcrumb>
               <Breadcrumb.Item linkAs={Link} linkProps={{ to: "/" }}>
                  Home
               </Breadcrumb.Item>
            <Breadcrumb.Item active>All Products</Breadcrumb.Item>
         </Breadcrumb>
            <h1 className={`${classes.h1Styles}`}>Products</h1>
            <div className={`${classes.lgContainerWrapper}`}>
                <Card className="mt-3 mb-3 ml-auto mr-auto w-100">
                    <ListGroup  horizontal className={`d-flex justify-content-center ${classes.lgContainer}`}>
                        <Link onClick={CategoryChange} to={`/products/category/all`} className={`${classes.noLink}`}><Card.Header>Categories</Card.Header></Link>
                        <Link onClick={CategoryChange} to={`/products/category/seeds`} className={`w-100 ${classes.lg} ${lastSegment==='seeds' ? classes.selected : ""}`}><ListGroup.Item className={`w-100 ${classes.lg} ${lastSegment==='seeds' ? classes.selected : ""}`}>Seeds</ListGroup.Item></Link>
                        <Link onClick={CategoryChange} to={`/products/category/containers`} className={`w-100 ${classes.lg} ${lastSegment==='containers' ? classes.selected : ""}`}><ListGroup.Item className={`w-100 ${classes.lg} ${lastSegment==='containers' ? classes.selected : ""}`}>Containers</ListGroup.Item></Link>
                        <Link onClick={CategoryChange} to={`/products/category/hydroponics`} className={`w-100 ${classes.lg} ${lastSegment==='hydroponics' ? classes.selected : ""}`}><ListGroup.Item className={`w-100 ${classes.lg} ${lastSegment==='hydroponics' ? classes.selected : ""}`}>Hydroponics</ListGroup.Item></Link>
                        <Link onClick={CategoryChange} to={`/products/category/soil`} className={`w-100 ${classes.lg} ${lastSegment==='soil' ? classes.selected : ""}`}><ListGroup.Item className={`w-100 ${classes.lg} ${lastSegment==='soil' ? classes.selected : ""}`}>Soil</ListGroup.Item></Link>
                        <Link onClick={CategoryChange} to={`/products/category/tools`} className={`w-100 ${classes.lg} ${lastSegment==='tools' ? classes.selected : ""}`}><ListGroup.Item className={`w-100 ${classes.lg} ${lastSegment==='tools' ? classes.selected : ""}`}>Tools</ListGroup.Item></Link>
                    </ListGroup>
                </Card>
            </div>
            <Container>
                 {loadingSpinner}
            </Container>
            <div className="row">
                {JSX}
            </div>
        </div>
    )
}