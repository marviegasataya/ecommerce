import React, { useState, useEffect, useRef } from "react";
import {Button, Table, Modal, Form, OverlayTrigger, Tooltip} from 'react-bootstrap';
import Swal from 'sweetalert2';
import classes from './AdminView.module.css'
import AddProduct from './AddProduct'

export default function AdminView({productsData, fetchData}){
    // Modal Controllers
        // Add Product
        const openAdd = () => setShowAdd(true);
        const closeAdd = () => setShowAdd(false);
        const [showAdd, setShowAdd] = useState(false); 

        // Delete product
        const openDelete = (productId, isActive) =>{
            setShowDelete(true);
            setProductId(productId);
            setIsActive(isActive);
        }
        const closeDelete = () => setShowDelete(false);
        const [showDelete, setShowDelete] = useState(false); 
        const [productId, setProductId] = useState();
        const [isActive, setIsActive] = useState();

        // Update product
        // Set Useref variables
        const nameInputRef = useRef();
        const priceInputRef = useRef();
        const stockQtyInputRef = useRef();
        const descriptionInputRef = useRef();
        const brandInputRef = useRef();
        
        // Set States
        const [_id, set_id] = useState();
        const [name, setName] = useState();
        const [price, setPrice] = useState();
        const [stockQty, setStockQty] = useState();
        const [description, setDescription] = useState();
        const [brand, setBrand] = useState();
        const [images, setImages] = useState();
        const [notMountedImages, setNotMountedImages] = useState(true);

        // Get A reference to the form
        const formEl = useRef();
        // to populate data

        // Select Control handlers
        // Stores the Category Select Input value
        const [valueCat, setValueCat] = useState("default");
        // Stores the Subcategory Select Input value
        const [valueSubcat, setValueSubcat] = useState("default");
        const [isSubcatDisabled, setIsDisabled] = useState(true);
        // Prepares the Subcategory options JSX needed for rendering
        const [subCatOptionsJSX, setSubCatOptionsJSX] = useState([]);
        const [subCatOptionsData, setsubCatOptionsData] = useState([]);
        // Prepares the Images Input Additions JSX needed for rendering
        const imageCountInputRef = useRef();
        const [totalImages, setTotalImages] = useState(1);
        const [isAddImage, setIsAddImage] = useState(false);
        const [imageInputFeildsJSX, setimageInputFeildsJSX] = useState([]);
        const imageInputRefs = useRef([]);
        const imageAttributeInputRefs = useRef([]);

        const [showUpdate, setShowUpdate] = useState(false); 
        const closeUpdate = () => setShowUpdate(false);

        // Component Variables
        // This is teh JSX for the prodcuts in table body
        const [products, setProducts] = useState([]);
        const openUpdate = (data, id) =>{
            setName(data.name);
            setPrice(data.price);
            setStockQty(data.stockQuantity);
            setDescription(data.details.description);
            setBrand(data.details.brand);
            setShowUpdate(true);
            setValueSubcat(data.details.subCategory);
            setValueCat(data.details.category);
            SetSubCategoryOptions(data.details.category);
            setImages(data.details.images)
            set_id(id);
            // Mount images
            imageInputRefs.current = [];
            imageAttributeInputRefs.current = [];
            setTotalImages(data.details.images.length);
        };

        // This loads the data into the page in the table
        useEffect(()=>{
            const productsArr = productsData.map(product => {
                return(
                    <tr key={product._id}>
                        <td>{product.name}</td>
                        <td>₱{product.price}</td>
                        <td>{product.stockQuantity}</td>
                        <td>{product.details.description}</td>
                        <td>
                            <p>{`${product.details.brand} Brandname under ${product.details.category} - ${product.details.subCategory}`}</p>
                        </td>
                        <td>{product.details.images.length}</td>
                        <td>{product.ratings}</td>
                        {/* <td>{product.ratings.length === 0 ? 'No Ratings': 'Display Ratings'}</td> */}
                        <td className={product.isActive ? "text-success" : "text-danger"}>
                            {product.isActive ? 'Active' : 'Disabled'}
                        </td>
                        <td>
                                <Button variant="primary" size="sm" onClick={()=>openUpdate(product, product._id)}>Update</Button>
                                {
                                    product.isActive ?
                                    <Button variant="danger" size="sm" onClick={
                                        () => openDelete(product._id, product.isActive)}>
                                        Disable
                                    </Button>
                                    :
                                    <Button variant="success" size="sm" onClick={
                                        () => openDelete(product._id, product.isActive)}>Enable</Button>
                                }
                        </td>
                    </tr>
                )
            });
            setProducts(productsArr);

        },[productsData]); // eslint-disable-line react-hooks/exhaustive-deps

/*===================================================== */
/*           DELETE MODAL FUNCTIONS         */
/*===================================================== */

    // This Archives the selected product
    const archiveToggle = ()=>{
        fetch(`${process.env.REACT_APP_API_URL}/products/archive/${productId}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify({
                isActive: !isActive
            })
        })
        .then(res => res.json())
        .then(data => {
            if( data.error !== 'error'){
                fetchData();
                Swal.fire({
                    title: data.title,
                    icon: 'success',
                    text: data.message
                })
                closeDelete();
            }else {
                fetchData();
                Swal.fire({
                    title: 'Something went wrong',
                    icon: 'error',
                    text: 'Please try again.'
                })
            }
        })
    }

/*===================================================== */
/*           UPDATE MODAL FUNCTIONS         */
/*===================================================== */
    if(notMountedImages){
        imageInputRefs.current = [];
        imageAttributeInputRefs.current = [];
    }

    // Updates the image feilds array JSX  everytime the modal is opened
    // Clears data when modal is closed
    useEffect(()=>{
        if(showUpdate){
            let imageFeilds = createInputJSX(totalImages);
            setimageInputFeildsJSX([...imageFeilds]);
        } else {
            setNotMountedImages(true);
        }
    }, [showUpdate]); // eslint-disable-line react-hooks/exhaustive-deps

    // Updates the image feilds array JSX  everytime  total image count is increased or decreased
    useEffect(()=>{
        if(!notMountedImages){
            let imageFeilds = createInputJSX(totalImages);
            setimageInputFeildsJSX([...imageFeilds]);
        }
    },[totalImages]) // eslint-disable-line react-hooks/exhaustive-deps

    // Mounts the values from the database into the image fields
    useEffect(()=>{
        if(totalImages === imageInputRefs.current.length && notMountedImages){

            for(let x = 0; x < images.length; x++){

                imageInputRefs.current[x].defaultValue = images[x].imageUrl;
                imageAttributeInputRefs.current[x].defaultValue = images[x].attribute;
            }
            setNotMountedImages(false);
        }

    }, [imageInputRefs.current, imageAttributeInputRefs.current]); // eslint-disable-line react-hooks/exhaustive-deps

    // Creates an array of JSX Input elements based on passed totalImages
    const createInputJSX = () => {
                // console.log(`Total Image Fields ${totalImages}`);
                const imageFeildCount = totalImages-1;
                let imageFeilds = [];
        
                // Value to populate the JSX with at least 1 imput feild
                const firstImageInput = (
                    <tr className='mb-1' key={0}>
                        <td colSpan={1} className={classes.numberLeft}>
                            <p className={classes.centerP}>1</p>
                        </td>
                        <td colSpan={9} className="pb-3">
                            <Form.Label column='sm'>Image Url: </Form.Label>
                            <Form.Control type="text" size='sm' placeholder="Add URL path of image" ref={addToImageRefs} required />
                            <Form.Label column='sm'>Image description: </Form.Label>
                            <Form.Control type="text" size='sm' placeholder="Add alt description" ref={addToImageAttributeRefs} required />
                        </td>
                        <td colSpan={2} className='d-flex flex-column align-items-center'>
                            <Button size='sm' className={(totalImages >= 1) ? classes.hideButton : 'mt-2'} onClick={removeImageFields}>
                                X
                            </Button>
                        </td>
                    </tr>
                );
                
                // Push JSX & Image Refs/ Attributes into their respective arrays
                imageFeilds.push(firstImageInput)
                
                for(let x = 0; x < imageFeildCount; x++){
                    // console.log(`adding image field`);
                    imageFeilds.push(
                        <tr className='mb-1' key={x+2}>
                            <td colSpan={1} className={classes.numberLeft}>
                                <p className={classes.centerP}>{x+2}</p>
                            </td>
                            <td colSpan={9} className="pb-3">
                            <Form.Label column='sm'>Image Url: </Form.Label>
                            <Form.Control type="text" size='sm' placeholder="Add URL path of image" ref={addToImageRefs} required/>
                            <Form.Label column='sm'>Image description: </Form.Label>
                            <Form.Control type="text" size='sm' placeholder="Add alt description" ref={addToImageAttributeRefs} required/>
                            </td>
                            <td colSpan={2} className='d-flex flex-column align-items-center'>
                                <Button size='sm' className={(totalImages >= x+3) ? classes.hideButton : 'mt-2'} onClick={removeImageFields}>
                                    X
                                </Button>
                            </td>
                        </tr>
                    )
                }

        return imageFeilds;
    }



    // Keeps track of total count of image feilds, decreases count
    const removeImageFields = (e) => {
        if(totalImages-1 < 8){
            setIsAddImage(false);
        }
        setTotalImages(totalImages - 1);
    }

    // dynamically creates and adds a reference attribute for a newly added input feils
    const addToImageRefs = (el) => {
        if(imageInputRefs.current){
            if(el && !imageInputRefs.current.includes(el)){
                imageInputRefs.current.push(el);
            }
        } else {
            imageInputRefs.current.push(el);
        }
    }

    // dynamically creates and adds a reference attribute for a newly added input feils
    const addToImageAttributeRefs = (el) => {
        if(imageAttributeInputRefs.current){
            if(el && !imageAttributeInputRefs.current.includes(el)){
                imageAttributeInputRefs.current.push(el);
            }
        } else {
            imageAttributeInputRefs.current.push(el);
        }

    }

    // Keeps track of total count of image feilds, increases count
    const addImageFields = (e) => {
        let additions = parseInt(imageCountInputRef.current.value);
        let runningTotal = totalImages;

        if(additions < 0){
            additions = 0;
        } 
        runningTotal += additions;
        if(runningTotal > 8){
            runningTotal = 8;
            setIsAddImage(true);
        }
        setTotalImages(runningTotal);
    }


    // Creates an array of JSX select options based on subCatOptionsData
    // Onchange of Category, the handleCategoryChange triggers which triggers this effect
    // since subCatOptionsData changes in handleCategoryChange() function
    useEffect(()=>{
        const optionsArr = subCatOptionsData.map(option =>{
            return(
                <option key={option} value={option}>{option}</option>     
            )
        });
        setSubCatOptionsJSX([...optionsArr]);
    }, [subCatOptionsData])

    // This function accepts an array and fills it with the appropriate values.
    const SetSubCategoryOptions = (category) => {
        let subcategoryTitles = [];
        switch(category){
            case 'Seeds':
                subcategoryTitles.push("Flowers");
                subcategoryTitles.push("Vegetables");
                subcategoryTitles.push("Fruits");
                break;
            case 'Containers':
                subcategoryTitles.push("Pots");
                subcategoryTitles.push("Seed/Grow bag");
                subcategoryTitles.push("Seedling Tray");
                break;
            case 'Hydroponics':
                subcategoryTitles.push("Hydroponics Kit");
                subcategoryTitles.push("Hydroponics Solution");
                subcategoryTitles.push("Hydroponics Accessories");
                break;
            case 'Soil & Amendments':
                subcategoryTitles.push("Soil & Growing Medium");
                subcategoryTitles.push("Solid Fertilizer");
                subcategoryTitles.push("Liquid Fertilizer");
                break;
            case 'Tools':
                subcategoryTitles.push("Grow Lights");
                subcategoryTitles.push("Shears");
                subcategoryTitles.push("Wires, Ties & Cages");
                subcategoryTitles.push("Watering Cans");
                break;
            default:
        }
        setValueSubcat(subcategoryTitles[0]);
        setsubCatOptionsData([...subcategoryTitles]);
        setIsDisabled(false);
    }

    // When Category Option value has been selected
    // It enables the Subcategory Select Input
    // It Stores the selected value in setValueCat
    const handleCategoryChange = (e) => {
            setValueCat(e.target.value);
            setsubCatOptionsData([]);
            SetSubCategoryOptions(e.target.value);
    };

    // Collects the value from the Subcategory Input Select Option
    const handleSubcategoryChange = (e) => {
            setValueSubcat(e.target.value);
    };


    // Actions Taken Upon Submit
    // Save in DB, Clear Form, Close Modal & Sweet Alert Pop up
    const submitHandler = (e) => {
        e.preventDefault();

        // Clean data iput from gathered references
        const name = nameInputRef.current.value.trim();
        const price = priceInputRef.current.value;
        const stockQty = stockQtyInputRef.current.value;
        const description = descriptionInputRef.current.value.trim();
        const brand = brandInputRef.current.value.trim();

        // Store All Image Urls in a Javascript Object
        let imageURLData = [];
        let imageAttributesData = [];

        // loop through added references for image URLS
        imageInputRefs.current.forEach((el, index) => {
            imageURLData.push(el.value);
        })

        // loop through added references for image attributes
        imageAttributeInputRefs.current.forEach((el, index) => {
            imageAttributesData.push(el.value);
        })

        // create a Javascript object for images
        let imageObjectDataArr = [];

        for(let x = 0; x < totalImages; x++){
            let imageObjectData = {
                imageUrl: imageURLData[x],
                attribute: imageAttributesData[x]
            }
            imageObjectDataArr.push(imageObjectData);
        }

        // create a Javascript object to be used in the body's submission:
        const formData = {
            name: name,
            price: price,
            stockQuantity: stockQty,
            details: {
                description: description,
                brand: brand,
                category: valueCat,
                subCategory: valueSubcat,
                images: [...imageObjectDataArr]
            }
        }

        // Add Updated data to database
        fetch(`${process.env.REACT_APP_API_URL}/products/update/${_id}`, {
            method: 'PUT',
            headers:{
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify(formData)
        })
        .then( response => response.json())
        .then(data => {
            if(data.success){
                Swal.fire({
                    title: data.title,
                    icon: 'success',
                    text: data.message
                });
                formEl.current.reset();
                setTotalImages(1);
                imageInputRefs.current = [];
                imageAttributeInputRefs.current = [];
                closeUpdate();
                fetchData();
            }else{
                Swal.fire({
                    title: data.title,
                    icon: 'error',
                    text: data.message
                });
            }  
        });


    };

    return(
        <>
            <div className="text-center my-4">
                <h2>Admin Products</h2>
                <div className="d-flex justify-content-center">
                    <Button variant="primary" onClick={openAdd}>Add new Product</Button>
                </div>
            </div>
            <Table striped bordered hover responsive>
                <thead className="bg-dark text-light">
                    <tr>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Stock Qty</th>
                        <th>Description</th>
                        <th>Details</th>
                        <th>Total Images</th>
                        <th>Ratings</th>
                        <th>Active</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {products}
                </tbody>
            </Table>

        {/* { ADD MODALS HERE } */}
        <AddProduct closeAdd={closeAdd} showAdd={showAdd} fetchData={fetchData}/>

        {/* { DELETE MODAL HERE } */}
        <Modal show={showDelete} onHide={closeDelete} >
            <Modal.Header closeButton>
                    <Modal.Title>{isActive ? 'Confirm Disable Product' : 'Confirm Product Re-activaton'}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>{isActive ? 'Are you sure you want to archive?' : 'Are you sure you want to reactivate?'}</p>
            </Modal.Body>
            <Modal.Footer className='d-flex justify-content-center'>
                    <Button variant="secondary" onClick={closeDelete}>NO, KEEP</Button>
                    <Button variant={isActive ? "danger" : "success"} type="submit" onClick={archiveToggle}>
                        {isActive ? 'YES, ARCHIVE' : 'YES, ACTIVATE'}
                    </Button>
            </Modal.Footer>
        </Modal>

        {/* { UPDATE MODAL HERE } */}
        <Modal show={showUpdate} onHide={closeUpdate}>
                <Modal.Header closeButton>
                        <Modal.Title>Update Product</Modal.Title>
                </Modal.Header>
                <Form onSubmit={submitHandler} ref={formEl}>
                    <Modal.Body>
                        <Form.Group> 
                            <Form.Label >Name</Form.Label>
                            <Form.Control type="text" placeholder="Enter name of product" size="sm" ref={nameInputRef} required defaultValue={name}/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label >Brand</Form.Label>
                            <Form.Control type="text" placeholder="Enter brand name" size="sm" ref={brandInputRef} required defaultValue={brand}/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Price</Form.Label>
                            <Form.Control type="number" size="sm" ref={priceInputRef} required defaultValue={price}/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Stock Qty</Form.Label>
                            <Form.Control type="number" size="sm" ref={stockQtyInputRef} required defaultValue={stockQty}/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Category</Form.Label>
                            <Form.Control as="select" size="sm" defaultValue={valueCat} onChange={handleCategoryChange}>
                                <option  value="default" disabled hidden>Select Category</option>
                                <option>Seeds</option>
                                <option>Containers</option>
                                <option>Hydroponics</option>
                                <option>Soil & Amendments</option>
                                <option>Tools</option>
                            </Form.Control>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Subcategory</Form.Label>
                            <Form.Control as="select" size="sm" defaultValue={valueSubcat} disabled={isSubcatDisabled} onChange={handleSubcategoryChange}>
                                <option  value="default" disabled hidden>Select Subcategory</option>
                                {subCatOptionsJSX}
                            </Form.Control>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Description</Form.Label>
                            <Form.Control type="text" as="textarea" size="sm" placeholder="Enter product description" ref={descriptionInputRef} required defaultValue={description}/>
                        </Form.Group>
                        <Form.Group>
                            <Table responsive="sm"  size='sm'>
                                <thead>
                                    <tr>
                                        <th colSpan={12}>Images:</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    {imageInputFeildsJSX}

                                    <tr>
                                        <td colSpan={1} className="d-flex">
                                            <OverlayTrigger
                                                placement={'bottom'}
                                                overlay={
                                                    <Tooltip id={`max image warn`}>
                                                    8 images max.
                                                    </Tooltip>
                                                }
                                            >
                                            <Form.Control type="number" size="sm" ref={imageCountInputRef} className={classes.imageCountInput} defaultValue={1} />
                                            </OverlayTrigger>
                                        </td>
                                        <td colSpan={11}>
                                            <Button size='sm' className='mt-0' onClick={addImageFields} disabled={isAddImage}>
                                                Add More Fields
                                            </Button>
                                            <p className={(totalImages === 8) ? `d-block ${classes.warnMessageP}` : 'd-none'}>max reached</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </Table>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeUpdate}>Close</Button>
                        <Button variant="success" type="submit">Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    )
}