import {useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import {Nav, NavDropdown, Button} from 'react-bootstrap';
import { Link, NavLink, useHistory } from 'react-router-dom';
import LoginContext from '../store/login-context';
import classes from './AppNavBar.module.css'
import {useLocation} from 'react-router-dom'

export default function AppNavBar(){
    const history = useHistory();
    const location = useLocation();
    const {user, unsetUser} = useContext(LoginContext);
    
    const logout = () => {
        unsetUser();
        history.replace('/login');
    }

    const reDirectToCart = () => {
        history.replace('/cart');
    }

    let rightNav = (localStorage.getItem('accessToken')) ?
    (
        <>
        <Nav.Link onClick={logout}>Logout</Nav.Link>
        </>
    ) : 
    (
        <>
          <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
          <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
        </>   
    )

    const cartSwitch = (param)=>{
        switch(param) {
            case "admin":{
                  return (
                      <>
                      </>
                  );
               }
            case "guest":{
                return (
                    <>
                    </>
                );  
            }
            case null:{
                return (
                    <>
                    </>
                );  
            }
            default:
                  if(location.pathname === '/cart' || location.pathname === '/cart/'){
                    return (
                        <>
                        </>
                    );
                  }
                  return (
                    <Nav>
                        <Button onClick={reDirectToCart}>
                            Cart
                        </Button>
                    </Nav>
                  )

              
          }
    }
    
    const renderSwitch = (param)=> {
        switch(param) {
          case "admin":{
                return (
                    <>
                        <Nav.Link as={NavLink} to="/products/all">Products</Nav.Link>
                        <Nav.Link as={NavLink} to="/orders/all">Orders</Nav.Link>
                        <Nav.Link as={NavLink} to="/user">Users</Nav.Link>
                    </>
                );
             }
          default:
            return (
                <>
                    <NavDropdown title="Catalogue" id="collasible-nav-dropdown">
                        <NavDropdown.Item as={Link} to="/products/all">All Products</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/products/category/seeds">Seeds</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/products/category/containers">Containers</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/products/category/hydroponics">hydroponics</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/products/category/soil">Soil & Amendments</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/products/category/tools">Tools</NavDropdown.Item>
                    </NavDropdown>
                    {/* <NavDropdown title="By Subcategory" id="collasible-nav-dropdown">
                        <NavDropdown.Item as={Link} to="/products/subcategory">Vegetable seeds</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/products/subcategory">Flower seeds</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/products/subcategory">Fruit seeds</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/products/subcategory">Pots</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/products/subcategory">Seedling bag/tray</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/products/subcategory">Hydroponics kit</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/products/subcategory">Hydroponics solution</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/products/subcategory">Grow Lights</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/products/subcategory">Soil</NavDropdown.Item>
                        <NavDropdown.Item as={Link} to="/products/subcategory">Fertilizers</NavDropdown.Item>
                    </NavDropdown> */}
                </>
            );
        }
    }

    return(
        <Navbar variant="dark" expand="lg"  fixed="top" className={`${classes.color}`}>
            <Navbar.Brand as={Link} to="/">
                GardenSpace
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    {renderSwitch(user.role)}
                </Nav>
                <div className="ml-auto d-flex">
                    {cartSwitch(user.role)}
                    <Nav className="ml-auto">
                        {rightNav}
                    </Nav>
                </div>
            </Navbar.Collapse>
        </Navbar>
    )
}

