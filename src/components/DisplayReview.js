import { FaStar } from 'react-icons/fa'
export default function DisplayReview({rating, size}){
    return  (
            <div>
                {[...Array(5)].map((star, i)=>{
                    const ratingValue = i + 1;
                    return (
                        <label key={ratingValue}>
                            <input type="hidden" value={ratingValue} name="rating"/>
                            <FaStar size={size} color={ratingValue <= rating ? "#ffc107" : "e4e5e9"}/>
                        </label>
                    )
                })}
            </div>
    )
}
