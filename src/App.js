import './App.css';

// import react router dom files
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch, Redirect } from 'react-router-dom';

// import context files
import {LoginContextProvider} from './store/login-context';
import {CartContextProvider} from './store/cart-context';

// import components
import AppNavBar from './components/AppNavBar';
import {Container} from 'react-bootstrap';

// import pages
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Order from './pages/Order';
import Products from './pages/Products';
import User from './pages/User';
import Cart from './pages/Cart';
import ViewOrders from './pages/ViewOrders';
import SingleProduct from './pages/SingleProduct'


function App() {
  return (
    <LoginContextProvider>
      <CartContextProvider>
        <Router>
            <AppNavBar/>
            <div className='pt-5 pb-5 fullHeight'>
              <Container fluid>
                <Switch>
                  <Route exact path="/" component={Home}/>
                  <Route exact path="/login" component={Login}/>
                  <Route exact path="/register">
                    {
                      localStorage.getItem('role') === 'admin' || localStorage.getItem('role') === 'user' ?
                      <Redirect to="/" /> :
                      <Register/>
                    }
                  </Route>
                  <Route exact path="/products/category/*" component={Products}/>
                  <Route exact path="/products/item/*" component={SingleProduct}/>
                  <Route exact path="/orders/all" component={Order}/>
                  <Route exact path="/products/all" component={Products}/>
                  <Route exact path="/user/" component={User}/>
                  <Route exact path="/cart">
                    {
                      localStorage.getItem('role') === 'user' ? <Cart/> : <Redirect to="/" />
                    }
                  </Route>
                  <Route exact path="/orders/view">
                    {
                      localStorage.getItem('role') === 'user' ? <ViewOrders/> : <Redirect to="/" />
                    }
                  </Route>
                </Switch>
              </Container>
            </div>
        </Router>
      </CartContextProvider>
    </LoginContextProvider>
  );
}

export default App;
