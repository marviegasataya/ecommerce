import {Container, Card, ListGroup, ListGroupItem, Row, Col, Button} from 'react-bootstrap'
import { useState, useEffect } from 'react'

export default function ViewOrders(){
    const [orders, setOrders] = useState([]);

    const fetchOrders = () => {
        fetch( `${process.env.REACT_APP_API_URL}/order/all`,
        {
        method: 'GET',
        headers:{
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        }
        })
        .then(res => res.json())
        .then(data => {
            if(data.success){
                console.log('success')
                console.log(data.orders)
                setOrders(data.orders);

                console.log(data.orders[0].products[0].product.details.images[0].imageUrl)
            } else {
                console.log('fail')
                setOrders(null);
            }
        })
    }

    useEffect(()=>{
        fetchOrders();
    }, [])

    const theJSXiNeed = orders.map(order =>{
        return (
            <Card className="mt-3 mb-3" key={order._id}>
            <Row>
                <Col sm md={4}>
                    <Card.Img variant="top" src={order.products[0].product.details.images[0].imageUrl} className="w-100 h-100 "/>
                </Col>
                <Col sm md={8}>
                    <ListGroup className="list-group-flush">
                    <ListGroupItem>Order ID:{order._id}</ListGroupItem>
                    <ListGroupItem>Date Placed: {order.purchasedOn}</ListGroupItem>
                    <ListGroupItem>Total items: {order.totalItems}</ListGroupItem>
                    <ListGroupItem>Total Price: {order.totalAmount}</ListGroupItem>
                    <ListGroupItem>Status: {order.status}</ListGroupItem>
                    <ListGroupItem>
                        <Button>
                            Review
                        </Button>
                    </ListGroupItem>
                    </ListGroup>
                </Col>
            </Row>
        </Card>
        )
    })

    return(
        <Container className='mt-3 p-1'>
            <div>
                <h1 className="mt-3 mb-3 text-center">Your Orders</h1>
            </div>
            <div>
                {theJSXiNeed}
            </div>         
        </Container>
    )
}