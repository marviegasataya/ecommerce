import { useState, useEffect } from 'react'
import { Button, Container, Card, Row, Col, Image, Spinner} from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom'

export default function Cart(){
    const history = useHistory();
    const [cart, setCart] = useState([]);
    const [totalItems, setTotalItems] = useState([]);
    const [totalPrice, setTotalPrice] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [found, setFound] = useState(true);

    const fetchCartData = () => {
        fetch( `${process.env.REACT_APP_API_URL}/order/cart/get`,
        {
        method: 'GET',
        headers:{
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        }
        })
        .then(res => res.json())
        .then(data => {
            setCart(data.cart);

            if(data.cart.length > 0){
                // Spread cart array into temp variable
                const cart = [...data.cart];

                // Get each item quantity in cart
                const cartQty = cart.map(itemObj => itemObj.quantity);

                // Get each item price in cart
                const cartPrice = cart.map(itemObj => itemObj.product.price);

                // Get the sum of the array
                const reducer = (previousValue, currentValue) => previousValue + currentValue;
                const totalItems = cartQty.reduce(reducer);

                // Compute total price
                const totalPrice = cartPrice.reduce(function(r,a,i){return r+a*cartQty[i]},0)

                // Save total
                setTotalItems(totalItems);
                setTotalPrice(totalPrice.toFixed(2));
                setIsLoading(false);
            } else {
                    // Save total
                    setTotalItems(0);
                    setTotalPrice(0);
                    setIsLoading(false);
                    setFound(false);
            }
        })
    }

    useEffect( ()=>{
        fetchCartData();
    }, []);

    const loadingSpinner = (
        <Container className='d-flex flex-column justify-content-center align-items-center p-0'>
        <div>
            <Spinner animation="border" style={{color: "green"}}/>
        </div>
        <div className='ml-2 mt-2'>
            <p>Loading...</p>
        </div>
        </Container>
     );

     const notFoundJSX = (
        <h4 className="text-center">No items in cart! Please Continue shopping</h4>
     )

    // Set the top cordinate to 0
    // make scrolling smooth
    const scrollToTop = () => {
        window.scrollTo({
        top: 0,
        behavior: "smooth"
        });
    };

    const orderSubmitHandler = ()=> {
        console.log("Order Submit!");
        fetch( `${process.env.REACT_APP_API_URL}/order/create`,
            {
                method: 'POST',
                headers:{
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(data.success){
                Swal.fire({
                    title: data.title,
                    icon: 'success',
                    text: data.message
                })
                history.push('/orders/view');
            } else {
                Swal.fire({
                    title: data.title,
                    icon: 'error',
                    text: data.message
                })
            }
            // push user history to orders page
        })
    }


    const itemsJSX = (cart.map(item => {
        return (
            <Card className="mb-3" key={item._id} >
                <Card.Body>
                    <Row>
                        <Col s md="4">
                            <Image src={item.product.details.images[0].imageUrl} rounded 
                            className="w-100"/>
                        </Col>
                        <Col s md="8" className="d-flex flex-column justify-content-between">
                            <div className="p-0">
                                <Card.Title className="mt-3 mb-1">{item.product.name}</Card.Title>
                                <p className="p-0 m-0">View Item</p>
                            </div>
                            
                            <div className="d-flex"><h5 className="mr-2">Price:</h5>{item.product.price}</div>
                            <div className="d-flex"><h5 className="mr-2">Quantity:</h5>{item.quantity}</div>
                            <div>
                                <hr />
                                <div className="d-flex justify-content-between">
                                    <div className="d-flex"><h5 className="mr-2">Total Price:</h5>
                                    {item.product.price*item.quantity}</div>
                                    <Button variant="danger">REMOVE</Button>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Card.Body>
            </Card>
        )
    }))

    const pageJSX = (
        <>
            <Card className="w-100">
                <Row>
                    <Col md={12} lg={6}>
                        <Card.Body>
                            <Card.Title>Order Summary:</Card.Title>
                            <hr />
                            <Container>
                                <Row>
                                    <Col><h4>Total Items:</h4></Col>
                                    <Col><h4 className="text-right">{totalItems}</h4></Col>
                                </Row>
                                <Row>
                                    <Col><h4>Total Price:</h4></Col>
                                    <Col><h5 className="text-right">₱ {totalPrice}</h5></Col>
                                </Row>
                            </Container>
                        </Card.Body>
                    </Col>
                    <Col md={12} lg={6}>
                        <Card.Body>
                            <Card.Title>Pick-Up in Store</Card.Title>
                            <p style={{ fontSize: '0.8rem' }} className="mb-1">
                                 Orders will be ready for pick-up at the store an hour after the order was placed. Orders placed after 4:00PM will be ready the next day.
                            </p>
                            <div className="m-0">Store Location:</div>
                            <Button variant="primary" className="mt-3" onClick={orderSubmitHandler}>Place Order</Button>
                        </Card.Body>
                    </Col>
                </Row>
            </Card>
            {/* ITEMS GO HERE */}
            <div className="d-flex justify-content-between">
                <h3 className="mt-3 mb-3 ml-2">Your Items:</h3>
                <h6 className="mt-4 mb-3 mr-2 text-danger">CLEAR CART</h6>
            </div>
            
            {itemsJSX}
            <hr />
            <Button onClick={scrollToTop}>
                Back to Top
            </Button>
        </>
    )

    return(
        <Container>
            <Row className="mt-4 mb-3">
                <Col className="text-center">
                    <h1 >Your Cart</h1>
                </Col>
            </Row>
            <div className={`p-2`}>
                {isLoading ? loadingSpinner : ""}
            </div>
            {isLoading ? "" : (found ? pageJSX : notFoundJSX)}
        </Container>
    )
}