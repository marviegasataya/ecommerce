import { useRef, useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import {Form, Button, Container, Card, Spinner, Row, Col} from 'react-bootstrap';

import Swal from 'sweetalert2';
import LoginContext from '../store/login-context';

export default function Login(){
    const history = useHistory();
    const {setUser} = useContext(LoginContext);

    const [isLoading, setIsLoading] = useState(false);
    const emailInputRef = useRef();
    const passwordInputRef = useRef();

    const submitHandler = (event)=> {
        event.preventDefault();

        const email = emailInputRef.current.value.trim();
        const pass = passwordInputRef.current.value.trim();

        const loginData = {
            email: email,
            password: pass
        }

        setIsLoading(true);
        fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
            method: 'POST',
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(loginData)
        })
        .then( response => response.json())
        .then( data => {
            setIsLoading(false);
            if(data.success){
                Swal.fire({
                    title: data.title,
                    icon: 'success',
                    text: data.message
                });
                localStorage.setItem('accessToken', data.token);
                setUser({ accessToken: data.token, email: email});
                emailInputRef.current.value = '';
                passwordInputRef.current.value = '';

                // Check user role
                fetch(`${process.env.REACT_APP_API_URL}/user`, {
                    headers:{
                        Authorization: `Bearer ${data.token}`
                    }
                })
                .then(res => res.json())
                .then(data => {
                    localStorage.setItem('role', data.user.role);
                    setUser({
                        role:  data.user.role
                    })
                    history.push('/');
                    // switch(data.user.role){
                    //     case 'admin':
                    //         history.push('/admin/dash');
                    //         break;
                    //     default:
                    //         history.push('/');
                    // }
                });

                // history.push('/');
            }else{
                Swal.fire({
                    title: data.title,
                    icon: 'error',
                    text: data.message
                });
            }            
        });
        
    }

    const redirectRegister = () => {
        history.push('/register');
    }

    let loadingSpinner = '';

    if(isLoading){
        loadingSpinner = 
        <Container className='d-flex flex-column justify-content-center align-items-center p-5'>
            <div>
                <Spinner animation="border"/>
            </div>
            <div className='ml-2 mt-2'>
                <p>Loading...</p>
            </div>
        </Container>
    }

    return(
        <Container className="mt-3">
            <Card>
                <Card.Header><h1 className="text-center">Login</h1></Card.Header>
                <Row>
                    <Col md={2}></Col>
                    <Col md={8}>
                        <Form onSubmit={submitHandler} className={isLoading ? 'd-none my-5' : 'd-block my-4'}>           
                        <Container>
                            <Form.Group>
                                <Form.Label>Email Address:</Form.Label>
                                <Form.Control type="email" placeholder="Enter Email"  required ref={emailInputRef}></Form.Control>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Password:</Form.Label>
                                <Form.Control type="password" placeholder="Enter Password" required ref={passwordInputRef}></Form.Control>
                            </Form.Group>
                            <Button  className="mb-3" variant="primary" type="submit">
                                Submit
                            </Button>
                        </Container>
                        </Form>
                    </Col >
                    <Col md={2}></Col>
                </Row>
                {
                    loadingSpinner
                }
                <Card.Footer className="text-muted text-center">Don't have an account yet? <Card.Link onClick={redirectRegister}>Click here</Card.Link> to register.</Card.Footer>
            </Card>
        </Container>   
    )
}