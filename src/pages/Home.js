import { useContext } from 'react'
import LoginContext from '../store/login-context';

// import views
import AdminView from '../components/home-views/AdminView';
import UserView from '../components/home-views/UserView';
import GuestView from '../components/home-views/GuestView';
import { Container } from 'react-bootstrap';

export default function Home(){
    const {user} = useContext(LoginContext);

    const renderSwitch = (param)=> {
        switch(param) {
          case "admin":{
                return <AdminView/>;
             }
          case "user":{
                return <UserView/>;
             }
          default:
            return <GuestView/>;
        }
    }

    return(
        <Container className='mt-3 p-1'>
           {renderSwitch(user.role)}
        </Container>
    )
}