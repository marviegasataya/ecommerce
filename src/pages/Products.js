import { useContext, useEffect, useState } from 'react'
import LoginContext from '../store/login-context';

// import views
import AdminView from '../components/product-views/AdminView';
import UserView from '../components/product-views/UserView';
// import { Container } from 'react-bootstrap';


export default function Products(){
    const {user} = useContext(LoginContext);
    const [allProducts, setAllProducts] = useState([]);


    // This function extracts all products from the DB
    const fetchData = () => {
        // check auth header sent
        fetch(`${process.env.REACT_APP_API_URL}/products/`, {
            method: 'GET',
            headers:{
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then(res => res.json())
        .then(data =>{
            setAllProducts(data.products);
        })
    }

    useEffect( ()=>{
        fetchData();
    }, []);

        // This function renders the correct view based on the user's role
        const renderSwitch = (param, fetchData)=> {
            switch(param) {
            case "admin":{
                    return <AdminView productsData={allProducts} fetchData={fetchData}/>;
                }
            default:
                return <UserView productsData={allProducts} fetchData={fetchData}/>;
            }
        }

    return(
        <div className='mt-3'>
           {renderSwitch(user.role, fetchData)}
        </div>
    )
}