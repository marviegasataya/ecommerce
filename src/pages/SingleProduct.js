import React, {useContext, useRef} from "react";
import {Row, Col, Carousel, Form, Button, InputGroup, FormControl, Spinner, Container, Image, Modal, Breadcrumb} from 'react-bootstrap'
import { useEffect, useState } from 'react'
import { useLocation, Link, useHistory } from 'react-router-dom'
import classes from './SingleProduct.module.css'
import DisplayReview from '../components/DisplayReview'
import Swal from 'sweetalert2';
// import { BSHeart, BsHeartFill } from 'react-icons/fa'

import CartContext from '../store/cart-context'

export default function SingleProduct(){
   const history = useHistory();

   const cartCtx = useContext(CartContext);  // abandon ship
   const location = useLocation();
   const lastSegmentString = location.pathname.split("/").pop();
   const [isLoading, setIsLoading] = useState(true);
   const [notFound, setNotFound] = useState(false);
   const [fetchedData, setfetchedData] = useState();
   const [carouselItems, setCarouselItems] = useState([]);
   // Show Img Modal
   const openImgModal = (imageUrl) =>{
      setShowModalImage(imageUrl);
      setShowImgModal(true);
   }
   const closeImgModal = () => setShowImgModal(false);
   const [showImgModal, setShowImgModal] = useState(false); 
   const [modalImage, setShowModalImage] = useState(); 

   const itemQuantity = useRef();
   
   useEffect(()=>{
      // cartCtx.setCart(cartCtx.cart);
      fetch(`${process.env.REACT_APP_API_URL}/products/${lastSegmentString}`, {
         method: 'GET',
         headers:{
             'Content-Type': 'application/json',
             Authorization: `Bearer ${localStorage.getItem('accessToken')}`
         }
     })
     .then(res => res.json())
     .then(data =>{
        if(data.success){
            setfetchedData(data.product);
            // Create Carousel items JSX
            const carouselJSX = data.product.details.images.map( image =>{
               return(
                     <Carousel.Item key={image._id}>
                        <img
                           className="d-block w-100"
                           src={image.imageUrl}
                           alt={image.attribute}
                        />
                     </Carousel.Item>
               )
            })
            setCarouselItems(carouselJSX);
        } else {
            setNotFound(true);
        }
         setIsLoading(false);
     })
   },[lastSegmentString])

   const addToCart = async (event)=> {
      event.preventDefault();

      const itemData = {
         _id: lastSegmentString,
         quantity: parseInt(itemQuantity.current.value)
     }

     fetch( `${process.env.REACT_APP_API_URL}/order/cart/add`,
     {
        method: 'PUT',
        headers:{
           'Content-Type': 'application/json',
         Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        },
        body: JSON.stringify(itemData)
     })
      .then(res => res.json())
      .then(data => {
         
         // Spread cart array into temp variable
          const cart = [...data.cart];

          // Get each item quantity in cart
          const cartQty = cart.map(itemObj => itemObj.quantity);

          // Get the sum of the array
          const reducer = (previousValue, currentValue) => previousValue + currentValue;
          const totalItems = cartQty.reduce(reducer);
          console.log(totalItems);

          cartCtx.setTotalItems(totalItems);

         //  ADD SWEET ALERT HERE
              if(data.success){
                 console.log(data.quantity);
                 // CUSTOM SWAL
                  Swal.fire({
                     title: '<strong>Item Added to Cart!</u></strong>',
                     html:
                     `<div class="row mt-3 mb-3 p-2"><div class="col">`+
                     `<img src="${data.product.details.images[0].imageUrl}" alt="${data.product.details.images[0].attribute}" style="height:200px;width:200px"/>`+
                     `</div><div class="col p-0">`+
                     `<h4>${data.product.name}</h4>` +
                     `<p>Qty added: ${data.quantity}</p>` +
                     `<p>Price per item: </p>` +
                     `<p>${data.product.price}</p>` +
                     `</div></div>`,
                     showCloseButton: true,
                     showCancelButton: true,
                     focusConfirm: false,
                     confirmButtonText:'View Cart',
                     cancelButtonText:
                     'Shop More',
                  })
                  .then((result) => {
                     if (result.isConfirmed) {
                        // PUSH TO HISTORY TO CART PAGE
                        history.push('/cart');
                     }
                   })
              } else {
                  Swal.fire({
                     title: data.title,
                     icon: 'error',
                     text: data.message
                  });
              }
      })
            
         
     



   }

   let prodJSX = (
         <Row>
            <Col sm={12} md={6}>
               <Carousel interval={null}>
                  {carouselItems}
               </Carousel>
            </Col>

            <Col sm={12} md={6}>
               <h2>{fetchedData ? fetchedData.name : ""}</h2>
               <div className="d-flex align-items-center">
                  <DisplayReview rating={fetchedData ? fetchedData.ratings : 0} size={20}/>
                  <span className="ml-2">
                     {fetchedData ? fetchedData.ratings : 0} Ratings
                  </span>
               </div>
               
               <h5>{fetchedData ? fetchedData.details.brand : ""}</h5>
               <h3>PHP {fetchedData ? fetchedData.price : ""}</h3>
               
               <Form className={`mt-3 ${classes.form}`} onSubmit={addToCart}>
               <p>{fetchedData ? fetchedData.details.description.slice(0, 150) : ""}</p>
               <InputGroup className={`mb-3 ${classes.qty}`}>
                  <InputGroup.Prepend>
                     <Button variant="outline-secondary">-</Button>
                  </InputGroup.Prepend>
                  <FormControl type="number" className="text-center" aria-label="Amount" defaultValue={1}  ref={itemQuantity}/>
                  <InputGroup.Append>
                     <Button variant="outline-secondary">+</Button>
                  </InputGroup.Append>
               </InputGroup>
                  <Button variant="primary" type="submit" >Add To Cart</Button>
               </Form>
               {/* <div>Wishlist</div>   */}
            </Col>
         </Row>
      )

   
      
   let ratingsJSX = (
      <Row>
         <Col>
            <div >
               <h4>Ratings & Reviews</h4>
            </div>
            <hr />
            {/* This is the JSX Template below for the comments and pics */}
            <div>
               <div>
                  <DisplayReview rating={4} size={15}/>
                  <p>by UserName</p>
                  <p>Comments</p>
                  <Row>
                     <Col>
                        <Button className={`${classes.imgBTN}`} onClick={()=>{openImgModal("https://via.placeholder.com/750?text=Strawberry")}}>
                           <Image className={`${classes.img}`} src="https://via.placeholder.com/750?text=Strawberry" rounded />
                        </Button>   
                        <Button className={`${classes.imgBTN}`} onClick={()=>{openImgModal("https://i.imgur.com/RInQwWl.jpg")}}>
                           <Image className={`${classes.img}`} src="https://i.imgur.com/RInQwWl.jpg" rounded />
                        </Button>                       
                     </Col>
                  </Row>
               </div>
               <hr />
            </div>
            <div>
               <div>
                  <DisplayReview rating={3} size={15}/>
                  <p>by UserName</p>
                  <p>Comments</p>
                  <Row>
                     <Col>
                        <Button className={`${classes.imgBTN}`} onClick={()=>{openImgModal("https://via.placeholder.com/750?text=Strawberry")}}>
                           <Image className={`${classes.img}`} src="https://via.placeholder.com/750?text=Strawberry" rounded />
                        </Button>   
                        <Button className={`${classes.imgBTN}`} onClick={()=>{openImgModal("https://i.imgur.com/RInQwWl.jpg")}}>
                           <Image className={`${classes.img}`} src="https://i.imgur.com/RInQwWl.jpg" rounded />
                        </Button>                       
                     </Col>
                  </Row>
               </div>
               <hr />
            </div>
         </Col>
      </Row>
   )

   const loadingSpinner = (
      <Container className='d-flex flex-column justify-content-center align-items-center p-0'>
      <div>
          <Spinner animation="border" style={{color: "green"}}/>
      </div>
      <div className='ml-2 mt-2'>
          <p>Loading...</p>
      </div>
      </Container>
   );

   const notFoundJSX = (
      <div>Not Found</div>
   )

   return  (
      <div className={`mt-3 ${classes.wrapper}`}>
         <div>
         <Breadcrumb>
               <Breadcrumb.Item linkAs={Link} linkProps={{ to: "/" }}>
                  Home
               </Breadcrumb.Item>
               <Breadcrumb.Item linkAs={Link} linkProps={{ to: "/products/all" }}>
                  All Products
               </Breadcrumb.Item>
               <Breadcrumb.Item linkAs={Link} linkProps={{ to: "/products/category/seeds" }}>
                  Seeds
               </Breadcrumb.Item>
               <Breadcrumb.Item linkAs={Link} linkProps={{ to: "/products/category/seeds" }}>
                  Vegetable Seeds
               </Breadcrumb.Item>
            <Breadcrumb.Item active>Petchay Seeds</Breadcrumb.Item>
         </Breadcrumb>
         </div>
         <div className={`bg-white p-2`}>
             {isLoading ? loadingSpinner : (notFound ? notFoundJSX : prodJSX)}
         </div>
         <div className={`bg-white p-3 mt-4`}>
             {isLoading ? "" : ratingsJSX}
         </div>
            {/* { DELETE MODAL HERE } */}
            <Modal show={showImgModal} onHide={closeImgModal} >
              <Modal.Header closeButton={true}>
                  <h6>Image view</h6>
              </Modal.Header>
              <Modal.Body>
               <Image className={`w-100`} src={modalImage} rounded />
              </Modal.Body>
            </Modal>
      </div>
   )
}
