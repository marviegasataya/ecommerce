import { useRef, useState } from 'react';
import { useHistory } from 'react-router-dom';
import {Form, Button, Container, Card, Spinner, Row, Col} from 'react-bootstrap';

import Swal from 'sweetalert2';

export default function Register(){
    const history = useHistory();

    const [isLoading, setIsLoading] = useState(false);
    const fullNameInputRef = useRef();
    const emailInputRef = useRef();
    const passwordInputRef = useRef();
    const verifyPasswordInputRef = useRef();

    const submitHandler = (event)=> {
        event.preventDefault();

        const fullName = fullNameInputRef.current.value.trim();
        const email = emailInputRef.current.value.trim();
        const pass = passwordInputRef.current.value.trim();
        const verifyPass = verifyPasswordInputRef.current.value.trim();


        const registrationData = {
            fullName:fullName,
            email: email,
            password: pass,
            verifyPassword: verifyPass
        }

        setIsLoading(true);
        fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
            method: 'POST',
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(registrationData)
        })
        .then( response => response.json())
        .then( data => {
            setIsLoading(false);
            if(data.success){
                Swal.fire({
                    title: data.title,
                    icon: 'success',
                    text: data.message
                });

                fullNameInputRef.current.value = '';
                emailInputRef.current.value = '';
                passwordInputRef.current.value = '';
                verifyPasswordInputRef.current.value = '';
                redirectLogin ();
            }else{
                Swal.fire({
                    title: data.title,
                    icon: 'error',
                    text: data.message
                });
            }            
        });
        
    }

    const redirectLogin = () => {
        history.replace('/login');
    }

    let loadingSpinner = '';

    if(isLoading){
        loadingSpinner = 
        <Container className='d-flex flex-column justify-content-center align-items-center p-5'>
            <div>
                <Spinner animation="border"/>
            </div>
            <div className='ml-2 mt-2'>
                <p>Loading...</p>
            </div>
        </Container>
    }


    return(
        <Container className="mt-3">
            <Card>
                <Card.Header><h1 className="text-center">Register</h1></Card.Header>
                <Row>
                    <Col md={2}></Col>
                    <Col md={8}>
                        <Form onSubmit={submitHandler} className={isLoading ? 'd-none my-5' : 'd-block my-4'}>           
                        <Container>
                            <Form.Group>
                                <Form.Label>Full Name:</Form.Label>
                                <Form.Control type="text" placeholder="Enter Full Name"  required ref={fullNameInputRef}></Form.Control>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Email:</Form.Label>
                                <Form.Control type="email" placeholder="Enter Email"  required ref={emailInputRef}></Form.Control>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Password:</Form.Label>
                                <Form.Control type="password" placeholder="Enter Password" required ref={passwordInputRef}></Form.Control>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Verify Password:</Form.Label>
                                <Form.Control type="password" placeholder="Re-enter password"  required ref={verifyPasswordInputRef}></Form.Control>
                            </Form.Group>
                            <Button  className="mb-3" variant="primary" type="submit">
                                Submit
                            </Button>
                        </Container>
                        </Form>
                    </Col >
                    <Col md={2}></Col>
                </Row>
                {
                    loadingSpinner
                }
                <Card.Footer className="text-muted text-center">Already have an account? <Card.Link onClick={redirectLogin}>Click here</Card.Link> to login.</Card.Footer>
            </Card>
        </Container>   
    )
}